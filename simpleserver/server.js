const http = require('http');
const url = require('url');
const path = require('path');
const fs = require('fs');

const mimeTypes = {
	"html": "text/html",
	"jpeg": "image/jpeg",
	"jpg": "image/jpg",
	"png": "image/png",
	"js": "text/javascript",
	"css": "text/css"
}


http.createServer((req, res) => {
  const uri = url.parse(req.url).pathname;
  const filename = path.join(process.cwd(), unescape(uri));
  console.log('Loading ' + uri );
  console.log(filename);
  let stats;

  try{
  	stats = fs.lstatSync(filename);
  } catch(e){
  	res.statusCode = 404;
  	res.setHeader('Content-Type', 'text/plain');
  	res.end('404 not found \n');
  	return;
  }

  if (stats.isFile()) {
  	const mimeType = mimeTypes[path.extname(filename).split(".").reverse()[0]];
  	
  	res.statusCode = 200;
  	res.setHeader('Content-Type', 'text/html');

  	const fileStream = fs.ReadStream(filename);
  	fileStream.pipe(res);
  	//res.end(fileStream.pipe(res));
  } else if (stats.isDirectory()) {
  	res.statusCode = 302;
  	res.setHeader('Location', 'index.html');
  	res.end();

  } else {
  	res.statusCode = 500;
  	res.setHeader('Content-Type', 'text/plain');
  	res.end('500 not found \n');
  }
 
}).listen(3000);
